/// <reference types = "cypress"/>
describe('filtering',()=>{
    beforeEach(() =>{
        cy.visit('https://todomvc-app-for-testing.surge.sh')
        cy.get('.new-todo',{timeout : 6000}).type("Clean room {enter}")
        cy.get('.new-todo',{timeout : 6000}).type("Learn JavaScript {enter}")
        cy.get('.new-todo',{timeout : 6000}).type("Use Cypress  {enter}")
        cy.get(':nth-child(2) > .view > .toggle').click()
    })
    it('should filters "Active" todos',() => {
        cy.contains('Active').click()
        cy.get('.todo-list li').should('have.length',2)
    })
    it('should filters "Completed" todos',() => {
        cy.contains('Completed').click()
        cy.get('.todo-list li').should('have.length',1)
    })
    it('should filters "All" todos',() => {
        cy.contains('All').click()
        cy.get('.todo-list li').should('have.length',3)
    })
})